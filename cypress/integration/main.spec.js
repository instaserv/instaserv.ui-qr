/* eslint-disable no-undef */
// @eslint 
// @ts-nocheck
/// <reference types="cypress" />
const URL = "./demo/index.html";
const URL_INLINE = "./demo/index-inline.html";
const URL_NO_OP_ID = "./demo/index-no-opId.html";

beforeEach(() => {
    cy.visit(URL);
});

function interceptFakeOperationIdWithpendingOperation(opId = "FAKE_OPERATION_ID", as = "opRequest", routeHandler) {
    cy.intercept(
        { method: "GET", url: `**/qr/v1/public-operations/${opId}`, },
        {
            headers: {
                "access-control-allow-origin": window.location.origin,
                "Access-Control-Allow-Credentials": "true",
            },
            body: {
                "qrId": "rHr8W5FkYfgg53apAwP3XN.pLqLf",
                "operationType": "assignOrder",
                "orderId": null,
                "restaurantId": null,
                "assignmentReaction": null,
                "assignmentReactionDetails": null,
            },

        },routeHandler).as(as);
}
function interceptFakeOperationIdWithAssigned() {
    cy.intercept(
        { method: "GET", url: "**/qr/v1/public-operations/FAKE_OPERATION_ID", },
        {
            headers: {
                "access-control-allow-origin": window.location.origin,
                "Access-Control-Allow-Credentials": "true",
            },
            body: {
                "qrId": "eE6MMAYcPok4krbccXVRns.75tG4",
                "operationType": "assignOrder",
                "orderId": "FAKE_ORDER_ID",
                "restaurantId": null,
                "assignmentReaction": "null",
                "assignmentReactionDetails": "-",
            },
        });
}
function interceptFakeOrderRequest(id = "FAKE_ORDER_ID") {
    cy.intercept(
        { method: "GET", url: `**/retail/v1/orders/${id}`, },
        {
            headers: {
                "access-control-allow-origin": window.location.origin,
                "Access-Control-Allow-Credentials": "true",
            },
            body: {
                "id": "juMqu75ZYH5ShXUdsp4hiD-2367",
                "numericId": null,
                "version": 0,
                "mode": "capture",
                "externalId": "order_id_1609425652",
                "orderSessionId": null,
                "orderingPointId": null,
                "users": ["ox6fg1SrLkKvXCVMqbMhtD",],
                "transactions": [{
                    "id": "tf3fgQ2Ry5qL3QQhZC7mHq",
                    "createdAt": "2020-12-31T14:41:43.368Z",
                    "status": "assigned",
                },],
                "merchantId": "cZpJax4nukdrF8JxwjtyKA",
                "storeId": "juMqu75ZYH5ShXUdsp4hiD",
                "storeType": "eCommerce",
                "storeTitle": "Hello Modified again 3",
                "storeAddress": "address",
                "storeUrl": null,
                "storeLogoUrl": "https://qrpe-merchants-development.s3.us-west-1.amazonaws.com/eM4ANZnCNYKBqhNmcDSSTb-4HoMAoidPTbTESNHxarMQG/logo.png",
                "storeMcc": null,
                "storePhoneNumbers": null,
                "shippingDetails": {
                    "provider": null,
                    "providerId": null,
                    "trackingNumber": null,
                    "trackingUrl": null,
                    "status": null,
                    "statusText": null,
                    "type": "flat",
                    "optionId": null,
                }, "shippingAddress": null,
                "acceptablePayments": ["instaServPay",],
                "lines": [{
                    "lineId": "dMD6e45DnsDwvfja5gESyF",
                    "type": "sku",
                    "title": "Safari Unisex Maroon Taffik Anti-Scratch Large Suitcase",
                    "refundable": true,
                    "details": {
                        "id": "2065",
                        "parentId": null,
                        "externalId": null,
                        "unit": "quantity",
                        "quantity": 1,
                        "price": 7000,
                        "totalPrice": 7000,
                        "totalTax": 0,
                        "totalDiscount": 0,
                        "totalCouponsSavings": 0,
                        "attributes": [],
                        "imageUrl": "https://demo-ecommerce.instaserv.app/wp-content/uploads/2020/10/ezgif.com-gif-maker-4.jpg",
                    },
                },],
                "status": "assigned",
                "externalPosId": null,
                "currency": "USD",
                "taxes": [],
                "totalDiscounts": 0,
                "totalCouponsSavings": 0,
                "totalShippingCost": 800,
                "totalTax": 0,
                "totalPriceExcludingTax": 7000,
                "tip": 0,
                "totalPrice": 7800,
                "extra": {},
                "createdBy": "-",
                "createdByAppClientId": "development-demo-woocommerce-api-key",
                "createdAt": "2020-12-31T14:40:53.004Z",
                "updatedBy": null,
                "updatedByAppClientId": null,
                "updatedAt": "2020-12-31T14:41:43.369Z",
                "shippingOptions": [],
                "discountDescription": null,
                "isDev": true,
            },
        });
}

context("Popup mode", () => {

    it("click on button open and close pop up", () => {
        // open
        cy.contains("Open popup",).click();
        cy.get("#qrtizeWidgetWrapper");

        // close
        cy.get("#qrtize-plugin-widget .close",).click();
        cy.get("#qrtizeWidgetWrapper",).should("be.hidden");

        // open again
        cy.contains("Open popup",).click();
        cy.get("#qrtizeWidgetWrapper",).should("not.be.hidden");


        // close again
        cy.get("#qrtize-plugin-widget .close",).click();
        cy.get("#qrtizeWidgetWrapper",).should("be.hidden");

    });
    it("should display a qr code and SCAN ME", () => {
        interceptFakeOperationIdWithpendingOperation();
        // open
        cy.contains("Open popup",).click();
        cy.get("#qrtizeWidgetWrapper");
        cy.get("canvas")
            .invoke("attr", "width")
            .should("equal", "154");
        cy.get("canvas")
            .invoke("attr", "height")
            .should("equal", "154");
        cy.contains(".waiting", "Scan me").should("be.visible");
        // interceptFakeOrderRequest();
        // interceptFakeOperationIdWithAssigned();
        // cy.contains(".scaned", "Scaned").should("be.visible");
    });
    it("show NOT display a QR code and SCANED", () => {
        interceptFakeOrderRequest();
        interceptFakeOperationIdWithAssigned();
        cy.contains("Open popup",).click();
        cy.get("#qrtizeWidgetWrapper");
        cy.get("canvas").should("not.be.visible");
        cy.contains(".scaned", "Scaned").should("be.visible");
    });
    it("show allow to reinit", () => {
        interceptFakeOrderRequest();
        interceptFakeOperationIdWithAssigned();
        interceptFakeOperationIdWithpendingOperation("FAKE_OPERATION_ID_2");
        cy.contains("#popup-opened", "POPUP OPENED").should("not.exist");
        cy.contains("#popup-first-open", "POPUP FIRST OPENED").should("not.exist");

        cy.contains("Open popup",).click();
        cy.get("#qrtizeWidgetWrapper");
        cy.get("canvas").should("not.be.visible");
        cy.contains(".scaned", "Scaned").should("be.visible");
        cy.contains("#popup-opened", "POPUP OPENED");
        cy.contains("#popup-first-open", "POPUP FIRST OPENED");

        cy.get("#qrtize-plugin-widget .close",).click();
        cy.contains("#popup-opened", "POPUP CLOSED");
        cy.contains("reInit",).click();
        cy.contains("Open popup",).click();
        cy.contains("#popup-opened", "POPUP OPENED");
        cy.contains(".scaned", "Scaned").should("not.be.visible");
        cy.contains(".waiting", "Scan me").should("be.visible");
    });
    // it("Should not send request until an opId is provided", () => {
    //     interceptFakeOperationIdWithpendingOperation("",'neverHappenRequest');
    //     cy.visit(URL_NO_OP_ID);
    //     cy.wait(3000);
    //     cy.wait('neverHappenRequest').should('')
    // });
    it("visit another page ", () => {
        cy.visit(URL_INLINE);
    });
});
context("Inline Mode", () => {
    it("must not open a popup", () => {
        cy.visit(URL_INLINE);
        cy.contains("Open popup",).click();
        cy.get("#qrtizeWidgetWrapper",).should("not.exist");
        cy.get("#div-id-to-put-qr-code #qrtize-plugin-widget");
    });
});
