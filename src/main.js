import { render, reInitWidget, } from "./widget/view";
import { setPropsToConfig, getConfig, } from "./widget/config";

const supportedAPI = ["show-qr-code", "apply-operation", "init",]; // enlist all methods supported by API (e.g. `IS('event', 'user-login');`)
/**
    The main entry of the application
*/
function app(window) {
    console.log("#app");
    // all methods that were called till now and stored in queue
    // needs to be called now 
    let globalObject = window[window["instaServSdk-Widget"]];
    let queue = globalObject.q;
    if (queue) {

        for (var i = 0; i < queue.length; i++) {

            // // handle jquery bug where it calls script multiple times
            // if (queue[i][0].toLowerCase() === "init") {
            //     break;
            // }


            if (queue[i][0].toLowerCase() === "init") {
                console.log("QRTIZE init", queue[i][1].opId);
                reInitWidget();
                init(queue[i][1]);
                console.log("QRTIZE started", getConfig());
            }

            else {
                apiHandler(queue[i][0], queue[i][1]);
            }  
        }
    }

    window[window["instaServSdk-Widget"]] = apiHandler;
}

function init(config){
    reInitWidget();
    // setTimeout(reInitWidget, 1000);
    setPropsToConfig(extendObject(getConfig(), config));

}

/**
 * Method that handles all API calls
 */
function apiHandler(api, params) {
    if (!api) throw Error("API method required");
    api = api.toLowerCase();

    if (supportedAPI.indexOf(api) === -1) throw Error(`Method ${api} is not supported`);

    switch (api) {
    case "init":
        console.log("INIT WITH PARAMS", params);
        init(params);
        break;
    case "show-qr-code":
        render(params);
        break;
    case "apply-operation":
        setPropsToConfig(params);
        break;
    default:
        console.warn(`No handler defined for ${api}`);
    }
}

function extendObject(a, b) {
    for (var key in b)
        if ({}.hasOwnProperty.call(b, key))
            a[key] = b[key];
    return a;
}

app(window);