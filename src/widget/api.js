import { getConfig, } from "./config";

export function getUrlAndAuth() {
    const { url, opId, token, } = getConfig();
    const fullUrl = `${url}qr/v1/public-operations/${opId}`;
    return { fullUrl, token, };
}

export function getQRCode(fullUrl, token) {
    return fetch(fullUrl, {
        headers: {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
        },
        credentials: "include",
    })
        .then(res => res.json())
        .catch(err => err);
}

export function getOrderDetails(id) {
    const { url, token, } = getConfig();

    const headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bearer " + token,
    };

    return fetch(`${url}retail/v1/orders/${id}`, { headers, credentials: "include", })
        .then(res => res.json())
        .catch(err => err);
}