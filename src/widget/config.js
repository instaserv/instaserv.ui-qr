let defaultConfig = {
    /**
     * Qrtize ServerUrl
     */
    url: SERVER_URL,
    
    /**
     * url for universal links
     */
    qrUrl: QR_URL,
    
    /**
     * Auth token
     */
    auth: AUTH,

    /**
     * Request frequency
     */
    requestFrequency: 1000, // Five minutes

    /**
     * element where html is placed
     */
    el: undefined,
    type: undefined,
    size: undefined,
    /**
     * User Auth token
     */
    token: undefined,

    /**
     * OperationId
     */
    opId: undefined,

    /**
     * Hooks
     */
    onPaid: ()=>{},
    onScaned: ()=>{},
    onCanceled: ()=>{},
    onReturned: ()=>{},
    onOrderRequestError: ()=>{},
    onOpen: ()=>{},
    onClose: ()=>{},
    onFirstOpen: ()=>{},
};

let currentConfig = {...defaultConfig, };

export function setPropsToConfig(initConfig){
    currentConfig = {...defaultConfig, ...initConfig,};
}

export function getConfig(){
    return {...currentConfig,};
}