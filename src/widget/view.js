// @ts-ignore
import inlineTpl from "./tpl/inlineTpl.html";
// @ts-ignore
import popupTpl from "./tpl/popupTpl.html";
import { getQRCode, getUrlAndAuth, getOrderDetails, } from "./api";

import "./styles/popup.scss";
import "./styles/style.scss";
import "./styles/spinner.css";
import QRCode from "qrcode";
import { getConfig, } from "./config";

/**
 * @typedef {object} Transaction
 * @property {string} id
 * @property {string} createdAt
 * @property {'assigned' | 'paid' | 'created' | 'fulfilled' | 'canceled' | 'returned' | 'success' } status
 */

/**
 * @typedef {object} Order
 * @property {Array<Transaction>} transactions
 */

let gotOrderNumber = false;
let gotOrderApproved = false;
let firstOpen = false;

const WIDGET_ID = "qrtize-plugin-widget";
const STATUS_ATTRIBUTE ="data-qrtize-status";
const STATUS = {
    waiting: "waiting",
    scaned: "scaned",
    approved: "approved",
    inprogress: "inprogress",
    declined: "declined",
    failed: "failed",
};


export function render() {
    const { el, type, } = getConfig();
    renderWrapper(el, type,);
}

function renderWrapper(el, type,) {
    let parent, elements = [];
    const { requestFrequency, onOpen, onFirstOpen, } = getConfig();
    // convert plain HTML string into DOM elements
    let temporary = document.createElement("div");
    getQrRequestRepetition(requestFrequency);

    if (type === "popup") {
        onOpen(getConfig());
        if (!firstOpen){
            firstOpen= true;
            onFirstOpen(getConfig());
        }
        temporary.innerHTML = popupTpl;
        parent = document.getElementsByTagName("body")[0];
    } else {
        temporary.innerHTML = inlineTpl;
        if (el) {
            parent = document.getElementById(el);
            console.log("SETTING ELEMENT", parent, el);
        } else {
            console.log(new Error("Pls enter the element id for apending widget"));
        }
    }
    while (temporary.children.length > 0) {
        const previous = document.getElementById(WIDGET_ID);
        if (previous) {
            if (type==="popup") document.getElementById("qrtizeWidgetWrapper").style.display = "";
            break;
        }
        elements.push(temporary.children[0]);
        parent.appendChild(temporary.children[0]);
    }

    initEvents(type);
}

function initEvents(type) {
    if (type === "popup") {
        const el = document.querySelector("#qrtizeWidgetWrapper .close");
        el.addEventListener("click", () => {
            const { onClose, } = getConfig();
            onClose(getConfig());
            document.getElementById("qrtizeWidgetWrapper").style.display = "none";
        });
    }
}

function setWidgetStatus(status) {
    const el = document.getElementById(WIDGET_ID);
    
    if (!el) return;
    
    // if (el.classList.contains(status)) return;
    if (el.classList.contains(STATUS.approved)) return;
    
    el.classList.remove(STATUS.waiting);
    el.classList.remove(STATUS.approved);
    el.classList.remove(STATUS.declined);
    el.classList.remove(STATUS.failed);
    el.classList.remove(STATUS.inprogress);
    el.classList.remove(STATUS.scaned);
    el.classList.remove(STATUS.waiting);
    
    el.setAttribute(STATUS_ATTRIBUTE, status);
    el.classList.add(status);
}

function getWidgetStatus(){
    const el = document.getElementById(WIDGET_ID);
    return el.getAttribute(STATUS_ATTRIBUTE);
}

export function reInitWidget(){
    setWidgetStatus(STATUS.waiting);
    gotOrderNumber = false;
    gotOrderApproved = false;
}

/**
 * set widget status depending of order data
 * @param {Order} order 
 */
function setWidgetStatusFromOrder(order) {
    console.log("#setWidgetStatusFromOrder");
    const conf =  getConfig();
    // handle reinit
    if (!gotOrderNumber) return;
    if (!order.transactions || !order.transactions.length) {
        if (getWidgetStatus() === STATUS.scaned) return;
        setWidgetStatus(STATUS.scaned);
        return conf.onScaned(order);
    }
    if (order.transactions.some(transaction => transaction.status === "paid")) {
        if (getWidgetStatus() === STATUS.approved) return;
        setWidgetStatus(STATUS.approved);
        return conf.onPaid(order);
    }
    if (order.transactions.some(transaction => transaction.status === "success")) {
        gotOrderApproved = true;
        if (getWidgetStatus() === STATUS.approved) return;
        setWidgetStatus(STATUS.approved);
        return conf.onPaid(order);
    }
    if (order.transactions.some(transaction => transaction.status === "canceled")) {
        if (getWidgetStatus() === STATUS.declined) return;
        setWidgetStatus(STATUS.declined);
        return conf.onCanceled(order);
    }
    if (order.transactions.some(transaction => transaction.status === "returned")) {
        if (getWidgetStatus() === STATUS.declined) return;
        setWidgetStatus(STATUS.declined);
        return conf.onReturned(order);
    }
    if (getWidgetStatus() === STATUS.scaned) return;
    setWidgetStatus(STATUS.scaned);
    return conf.onScaned(order);
}

function getQrRequestRepetition(interval) {
    const { fullUrl, token, } = getUrlAndAuth();
    if (!token){
        setTimeout(() => { getQrRequestRepetition(interval); }, interval);
    }
    getQRCode(fullUrl, token)
        .then(resp => {
            if (gotOrderNumber) return;

            onGetQrCode(resp);
            setTimeout(() => { getQrRequestRepetition(interval); }, interval);
        }).catch(() => {
            console.log("#getQrRequestRepetition, error");
            getQrRequestRepetition(interval);
        });

}

function getGetOrderRequestRepetition(orderId) {
    console.log("#sendGetOrderRequestRepetition");
    getOrderDetails(orderId)
        .then(onGetOrderDetailsResponse)
        .catch(onGetOrderDetailsError)
        .finally(() => {
            if (gotOrderApproved) return;
            if (!gotOrderNumber) return;
            getGetOrderRequestRepetition(orderId);
        });
}

function onGetOrderDetailsResponse(response) {
    console.log("onGetOrderDetailsResponse");
    setWidgetStatusFromOrder(response);
}

function onGetOrderDetailsError(response) {
    getConfig().onOrderRequestError(response);
    console.log("onGetOrderDetailsError");
    console.log(response);

}

function onGetQrCode(data) {
    if (data.qrId) {
        const { size = 154, qrUrl, } = getConfig();
        /**
         * @type {HTMLCanvasElement}
         */
        const QRCodeDiv = document.querySelector(".QRCode");
        QRCodeDiv.addEventListener("click", () => {
            console.log("url-->",qrUrl + data.qrId);
            window.open(qrUrl + data.qrId);
        });

        QRCode.toCanvas(QRCodeDiv, qrUrl + data.qrId, { width: size, margin: 0, }, function (error) {
            if (error) console.error(error);
            console.log("success! " + data.qrId);
        });
    } else {
        console.error("qrId undefined");
    }
    if (data.orderId) {
        setWidgetStatus(STATUS.scaned);
        gotOrderNumber = true;
        getGetOrderRequestRepetition(data.orderId);

    }
}
