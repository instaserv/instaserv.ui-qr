const { Component } = require('@serverless/core');

class Website extends Component {
    async default(inputs = { env: 'development' }) {
        if (! ['development', 'staging', 'production'].includes(inputs.env)) {
            throw new Error('Invalid env passed, valid env values: development, staging, production')
        }

        const template = await this.load('@serverless/template', inputs.env);

        let domain;

        if (inputs.env === 'development') {
            domain = 'demo-qr-dev.instaserv.app';
        } else if (inputs.env === 'staging') {
            domain = 'demo-qr-staging.instaserv.app';
        } else if (inputs.env === 'production') {
            domain = 'demo-qr-production.instaserv.app';
        }

        const output = await template({
            template: {
                name: `${inputs.env}-qr-demo-web`,
                admin: {
                    component: '@serverless/website@4.0.0',
                    inputs: {
                        code: {
                            src: 'dist',
                            root: './'
                        },
                        env: {
                            enableSentry: true
                        },
                        region: `us-west-1`,
                        bucketName: `${inputs.env}-qr-widget-demo`,
                        domain
                    }
                }
            }
        });

        return output;
    }

    async remove(inputs = { env: 'development' }) {
        const website = await this.load('@serverless/template', inputs.env);

        await website.remove();

        return {};
    }
}

module.exports = Website;
