require('dotenv').config();
const FtpDeploy = require("ftp-deploy");
const fs = require('fs');

const excludePath = ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**", ".vscode/**"];
const includePath = ["*.*"];
const woocommercePath = __dirname + "/../../woocommerce.payment-plugin";

function ftp(activeStage) {
    const ftpDeploy = new FtpDeploy();
    console.log(activeStage);
    console.log(process.env.FTP_USER);
    console.log(process.env.FTP_PASSWORD);
    const config = {
        user: process.env.FTP_USER,
        password: process.env.FTP_PASSWORD,
        host: activeStage.ip,
        port: 21,
        localRoot: woocommercePath,
        remoteRoot: activeStage.remoteRoot,
        // include: ["*", "**/*"],      // this would upload everything except dot files
        include: includePath,
        // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
        exclude: excludePath,
        // delete ALL existing files at destination before uploading, if true
        deleteRemote: false,
        // Passive mode is forced (EPSV command is not sent)
        forcePasv: true,
        // use sftp or ftp
        sftp: false
    };

    ftpDeploy
        .deploy(config)
        .then(res => console.log("List of success uploaded files:", res))
        .catch(err => {
            console.error(err);
            process.exit(1)
        });
}

function sftp(activeStage) {
    // TODO: complete this part

    const cmd = `sftp ${activeStage.user}@${activeStage.ip} <<< $"put ${__dirname}/../../woocommerce.payment-plugin/* ${activeStage.remoteRoot}"`;
    // exec(cmd, (error, stdout, stderr) => {
    //     if (error) {
    //         console.log(`error: ${error.message}`);
    //         process.exit(1)
    //     }
    //     if (stderr) {
    //         console.log(`stderr: ${stderr}`);
    //         process.exit(1)
    //     }
    //     console.log(`stdout: ${stdout}`);
    // });

}

function writePhpConfigFile(activeStage) {
    const content = `<?php

    return array(
        'url' => '${activeStage.url}',
        'dashboardUrl' => '${activeStage.dashboardUrl}',
    );`;

    const filePath = `${woocommercePath}/includes/config.php`;

    fs.writeFile(filePath, content, (err) => {
        if (err) throw err;

        console.log('PHP config saved!');
    });
}

/**
 * 
 * declare vars
 * 
 */

const PROTOCOL_EXECUTORS = {
    SFTP: sftp,
    FTP: ftp,
}


const PROTOCOL_TYPES = {
    SFTP: "sftp",
    FTP: "ftp",
}

const stageConfig = {
    development: {
        ip: "devwoocommerce.qrtize.app",
        user: "bitnami", // just for SFTP
        remoteRoot: "/",
        executor: PROTOCOL_EXECUTORS.FTP,
        type: PROTOCOL_TYPES.FTP,
        url: 'https://dev-api.instaserv.app',
        dashboardUrl: 'https://merchant-dev.instaserv.app'

    },
    staging: {
        ip: "107.180.54.254",
        remoteRoot: "/",
        executor: PROTOCOL_EXECUTORS.FTP,
        type: PROTOCOL_TYPES.FTP,
        url: 'https://staging-api.instaserv.app',
        dashboardUrl: 'https://merchant-staging.instaserv.app'
    },
    qa: {
        ip: "qrpe.app",
        remoteRoot: "/",
        executor: PROTOCOL_EXECUTORS.FTP,
        type: PROTOCOL_TYPES.FTP,
        url: 'https://qa-api.qrpe.app',
        dashboardUrl: 'https://merchant-qa.qrpe.app'
    },
    production: {
        ip: "qrpe.app",
        remoteRoot: "/",
        executor: PROTOCOL_EXECUTORS.FTP,
        type: PROTOCOL_TYPES.FTP,
        url: 'https://api.qrpe.app',
        dashboardUrl: 'https://merchant.qrpe.app'
    }
}

function main() {
    const stage = process.argv[process.argv.length - 1];

    const activeStage = stageConfig[stage];

    if (!activeStage) {
        console.error("Wrong stage name");
        exit(0);
    }

    if (activeStage.type === PROTOCOL_TYPES.FTP) {
        if (!process.env.FTP_USER || !process.env.FTP_PASSWORD) {
            console.error("You need to set these variables ( USER, PASSWORD, SERVER ) correctly");
            exit(0)
        }
    }

    writePhpConfigFile(activeStage);

    console.log(`Start uploading the files to the server(${activeStage.ip})...`);

    activeStage.executor(activeStage);
}

main();