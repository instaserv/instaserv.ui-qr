const path = require("path");
const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin"); // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
const CopyWebpackPlugin = require("copy-webpack-plugin");

const appConfig = require("./app.config.js");
const bundleOutputDir = "./dist";
const isDev = (process.env.NODE_ENV === "development") ? true : false;

if (!process || !process.env || !process.env.NODE_ENV) {
    console.error("No NODE_ENV defined");
    process.exit(1);
}

if (!appConfig.hasOwnProperty(process.env.NODE_ENV)) {
    console.error("NODE_ENV is invalid");
    process.exit(2);
}

const activeAppConfig = appConfig[process.env.NODE_ENV];

const defaultPlugins = [
    new webpack.SourceMapDevToolPlugin(), 
    new webpack.DefinePlugin({
        ... activeAppConfig,
        GIT_COMMIT_ID: process.env.GIT_COMMIT_ID,
    }),
];

module.exports = {
    mode: isDev ? "development" : "production",
    entry: "./src/main.js",
    output: {
        filename: "widget.js",
        path: path.resolve(bundleOutputDir),
    },
    devServer: {
        host: "local.instaserv.app",
        port: 4200,
        contentBase: path.resolve(bundleOutputDir),
    },
    plugins: isDev ? [
        new CopyWebpackPlugin([{ from: "demo/", },]),
        ...defaultPlugins,
    ] : [
        new CopyWebpackPlugin([{ from: "demo/", },]),
        ...defaultPlugins,
    ],
    optimization: !isDev ? {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                extractComments: true,
                cache: true,
                parallel: true,
                terserOptions: {
                    extractComments: "all",
                    compress: {
                        drop_console: true,
                    },
                },
            }),
        ],
    } : {},
    module: {
        rules: [
            { test: /\.html$/i, use: "html-loader", },
            { test: /\.css$/i, use: ["style-loader", "css-loader",], },
            {test: /\.s[ac]ss$/i, use: ["style-loader", "css-loader", "sass-loader", ],},
            { test: /\.(png|jpe?g|gif|svg|ttf|woff|woff2)$/i,  use: [ {
                loader: "file-loader",
                options: {
                    esModule: false,
                },
            }, ],},
            {
                test: /\.js$/i, exclude: /node_modules/, use: {
                    loader: "babel-loader",
                    options: {
                        presets: [["@babel/env", {
                            "targets": {
                                "browsers": ["ie 6", "safari 7",],
                            },
                        },],],
                    },
                },
            },
            // {
            //     test: /\.(jpg|jpeg|gif|png|svg)$/,
            //     exclude: /node_modules/,
            //     loader:'url-loader?limit=1024&name=/wp-content/plugins/qrtize/assets/images/[name].[ext]',
            //     options: {
            //         esModule: false,
            //     },
            // },
            // {
            //     test: /\.(woff|woff2|eot|ttf)$/,
            //     exclude: /node_modules/,
            //     loader: 'url-loader?limit=1024&name=/wp-content/plugins/qrtize/assets/fonts/[name].[ext]'
            // }
        ],
    },
    devtool:"hidden-source-map",
};
