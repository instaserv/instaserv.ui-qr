module.exports = {
    development: {
        SERVER_URL: "'https://dev-api.instaserv.app/'",
        AUTH: "'Bearer development-woocommerce-plugin'",
        QR_URL: "'https://myqr-dev.instaserv.app/'",
    },
    staging: {
        SERVER_URL: "'https://staging-api.instaserv.app/'",
        AUTH: "'Bearer staging-woocommerce-plugin'",
        QR_URL: "'https://myqr-staging.instaserv.app/'",
    },
    qa: {
        SERVER_URL: "'https://qa-api.qrpe.app/'",
        AUTH: "'Bearer qa-woocommerce-plugin'",
        QR_URL: "'https://qr-qa.qrtize.app/'",
    },
    production: {
        SERVER_URL: "'https://api.qrpe.app/'",
        AUTH: "'Bearer production-woocommerce-plugin'",
        QR_URL: "'https://qr.qrtize.app/'",
    },
};